import React from 'react';
import {dishData} from './DishData';


function RestaurantSelector(props) {
    var classes = "centre " + (props.hasError ? "error":"");
    var availableOptions = props.restaurantDict;
    var optionTags = [];
    optionTags.push(<option key={0} value="">---</option>)
    for (var item in availableOptions){
        optionTags.push(<option key={item+1} value={availableOptions[item]}>{availableOptions[item]}</option>);
    }
    return(
        <select className={classes} 
                id="restaurant"
                value={props.restaurant}
                onChange={props.getRestaurantVal}>
            {optionTags}
        </select>
    );
};

class Stage2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            initRestaurant: true,
        }
        this.getRestaurantVal = this.getRestaurantVal.bind(this);
    };
    isRestaurantValid(){
        if (this.state.initRestaurant) { return true; }
        if (!this.props.restaurant){
            return false;
        }
        return true;
    };
    updateRestaurantDict(){
        return dishData.getRestaurantDict(this.props.meal)
    }
    getRestaurantVal(event) {
        this.setState({initRestaurant: false})
        this.props.updateModel({restaurant: event.target.value, dishes: [{name: "", numOrders: 1}]});
    };
    render() {
        return(
            <div className={"container-fluid " + (this.props.isHidden ? "swappedOut" : "")}>
                <div className="row">
                    <div className="field-label centre">Please Select a Restaurant</div>
                </div>
                <div className="row">
                    <RestaurantSelector getRestaurantVal={this.getRestaurantVal}
                                        restaurantDict={this.updateRestaurantDict()}
                                        restaurant={this.props.restaurant}
                                        hasError={!this.isRestaurantValid()}/>
                </div>
                <div className="row">
                    <small className={"text-danger centre " + (this.isRestaurantValid() ? "hidden" : "")}>
                        Please Select a Restaurant
                    </small>
                </div>
            </div>
        );
    };
};

export default Stage2;
