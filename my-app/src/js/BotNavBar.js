import React from 'react'

class BotNavBar extends React.Component {
    render() {
        var currentStage = this.props.currentStage;
        var prevButtonClass = "btn btn-outline-primary col-sm-2 " + (currentStage === 0 ? "hidden" : "");
        return(
            <div className="row">
                <button type="button" className={prevButtonClass}
                        onClick={() => this.props.changeStage(currentStage-1)}>
                    Previous
                </button>
                { 
                    currentStage === 3
                    ? <button type="button" className="btn btn-info offset-sm-8 col-sm-2"
                        onClick={() => this.props.submit()}>
                        Submit
                      </button> 
                    : <button type="button" className="btn btn-outline-primary offset-sm-8 col-sm-2"
                        onClick={() => this.props.changeStage(currentStage+1)}
                        disabled={this.props.maxStage < (currentStage+1)}>
                        Next
                      </button>
                }
            </div>
        );
    };
};

export default BotNavBar;
