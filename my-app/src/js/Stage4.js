import React from 'react';

class Stage4 extends React.Component {
    render() {
        var dishArr = this.props.dishes;
        var jsxDishes = [];
        for (var item in dishArr){
            jsxDishes.push(<li className="list-group-item list-group-item-light justify-content-between d-flex"
                               key={item}>
                                {dishArr[item].name}
                                <span className="badge badge-secondary badge-pill">x{dishArr[item].numOrders}</span>
                           </li>);
        }
        return(
            <div className={"container-fluid " + (this.props.isHidden ? "swappedOut" : "")}>
                <div className="row">
                    <div className="field-label col-sm-5">Meal</div>
                    <div className="col-sm-7">{this.props.meal.charAt(0).toUpperCase()+this.props.meal.substr(1)}</div>
                </div>
                <div className="row">
                    <div className="field-label col-sm-5">No. of People</div>
                    <div className="col-sm-7">{this.props.numPeople}</div>
                </div>
                <div className="row">
                    <div className="field-label col-sm-5">Restaurant</div>
                    <div className="col-sm-7">{this.props.restaurant}</div>
                </div>
                <div className="row">
                    <div className="field-label col-sm-5">Dishes</div>
                    <ul className="col-sm-7 list-group">
                        {jsxDishes}
                    </ul>
                </div>
            </div>
        );
    };
};

export default Stage4;
