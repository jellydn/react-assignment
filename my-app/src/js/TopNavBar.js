import React from 'react';

function TopNavButton(props) {
    var classes = "btn col-md-2 " + (props.isSelected ? "btn-secondary" : "btn-outline-secondary");
    return(
        <button type="button" className={classes}
                disabled={props.isAvailable}
                onClick={props.onClick}>
            {props.value}
        </button>
    );
};

class TopNavBar extends React.Component {
    renderButton(i) {
        return(
            <TopNavButton
                onClick={() => this.props.changeStage(i)}
                isSelected={this.props.currentStage === i}
                isAvailable={this.props.maxStage < i}
                value={i===3 ? "Review" : "Stage " + (i + 1)}
            />
        );
    };

    render() {
        return(
            <div className="row">
                {this.renderButton(0)}
                {this.renderButton(1)}
                {this.renderButton(2)}
                {this.renderButton(3)}
            </div>
        );
    };
};

export default TopNavBar;
