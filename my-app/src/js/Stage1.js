import React from 'react';

function MealSelector(props) {
    var classes = "centre " + (props.hasError ? "error":"");
    return(
        <select className={classes} 
                id="meal"
                value={props.meal}
                onChange={props.getMealVal}>
            <option value="">---</option>
            <option value="breakfast">Breakfast</option>
            <option value="lunch">Lunch</option>
            <option value="dinner">Dinner</option>
        </select>
    );
};

function NumPeopleInput(props) {
    var classes = "centre small-num-in " + (props.hasError ? "error":"");
    return(
        <input className={classes}
               type="number"
               value={props.numPeople}
               id="numPeople"
               onChange={props.handleNumChange}/>
    );
}

class Stage1 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            initNumPeople: true,
            initMeal: true
        }
        this.getMealVal = this.getMealVal.bind(this);
        this.handleNumChange = this.handleNumChange.bind(this);
        //this.isMealValid = this.isMealValid.bind(this);
        //this.isNumPeopleValid = this.isNumPeopleValid.bind(this);
    };
    
    isNumPeopleValid(){
        if (this.state.initNumPeople) { return true; }
        if (this.props.numPeople < 1 || this.props.numPeople > this.props.maxInputs){
            return false;
        }
        return true;
    }

    handleNumChange(event) {
        this.setState({initNumPeople: false})
        this.props.updateModel({numPeople: event.target.value});
    };

    isMealValid(){
        if (this.state.initMeal) { return true; }
        if (!this.props.meal){
            return false;
        }
        return true;
    }

    getMealVal(event) {
        this.setState({initMeal: false})
        this.props.updateModel({meal: event.target.value, dishes: [{name: "", numOrders: 1}], restaurant: ""});
    };

    render() {
        return(
            <div className={"container-fluid " + (this.props.isHidden ? "swappedOut" : "")}>
                <div className="row">
                    <div className="field-label centre">Please Select a Meal</div>
                </div>
                <div className="row">
                    <MealSelector getMealVal={this.getMealVal}
                                  meal={this.props.meal}
                                  hasError={!this.isMealValid()}/>
                </div>
                <div className="row">
                    <small className={"text-danger centre " + (this.isMealValid() ? "hidden" : "")}>
                        Please Select a Meal
                    </small>
                </div>
                <div className="row">
                    <div className="field-label centre">Please Enter Number of People</div>
                </div>
                <div className="row">
                    <NumPeopleInput handleNumChange={this.handleNumChange}
                                    numPeople={this.props.numPeople}
                                    maxInputs={this.props.maxInputs}
                                    hasError={!this.isNumPeopleValid()}/>                    
                </div>
                <div className="row">
                    <small className={"text-danger centre " + (this.isNumPeopleValid() ? "hidden" : "")}>
                        Please enter a number from 1 - {this.props.maxInputs}
                    </small>
                </div>
            </div>
        );
    };
};

export default Stage1;
