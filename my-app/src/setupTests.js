import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

/*
    In practice, a lot more global models and settings should be implemented, 
    so as to ensure tests inputs can be changed easily and test parameters are abstracted away
    from unnecessarily manual implementations (which are difficult to track and maintain for extension)

    This time around, as I was unfamiliar with Jest, the development cycle was backwards.
    I'm aware that a lot of devs actually advocate writing tests before coding the feature itself.
*/

const falseModel = {
    meal: "lunch",
    numPeople: 3,
    restaurant: "Ichiraku",
    dishes: [{name: "Shoyu", numOrders: 2}, {name: "Miso", numOrders: 1}, {name: "Tonkotsu", numOrders: 1}],
    maxInputs: 10
}

global.falseModel = falseModel;