import React from 'react';
import ReactDOM from 'react-dom';
import {shallow, mount} from 'enzyme';
import sinon from 'sinon';
import {dishData} from'../js/DishData';
import Stage3 from '../js/Stage3';

jest.mock('../js/DishData');

beforeAll(() => {
  dishData.getDishDict= (a,b) =>{
            return ["Offu","Teruhashi","san"];
  };
});

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Stage3 dishes={[]}
  						  checkNums={()=> {}}
  						  checkNames={()=> {}}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('renders appropriate list items', () => {
  const div =  mount(<Stage3 meal={falseModel.meal}
                               numPeople={falseModel.numPeople}
                               restaurant={falseModel.restaurant}
                               dishes={falseModel.dishes}
                               checkNums={()=> {}}
                               checkNames={()=> {}}/>);
  expect (div.find('option')).toHaveLength((falseModel.dishes.length+1)*3);
});

it('renders appropriate number of dish selection rows', () => {
  const div =  mount(<Stage3 meal={falseModel.meal}
                               numPeople={falseModel.numPeople}
                               restaurant={falseModel.restaurant}
                               dishes={falseModel.dishes}
                               checkNums={()=> {}}
                               checkNames={()=> {}}/>);
  expect (div.find('DishRow')).toHaveLength(falseModel.dishes.length);
});

/*the falsemodel's dish count is the same the mocked function.  
Perhaps a custom model should be defined here for better robustness and maintainability.
Also of note, the selectors are all very generic, as the component is simple. Better IDs should be used*/
it('hides the add button at the total number of dishes', () => {
  const div =  shallow(<Stage3 meal={falseModel.meal}
                               numPeople={falseModel.numPeople}
                               restaurant={falseModel.restaurant}
                               dishes={falseModel.dishes}
                               checkNums={()=> {}}
                               checkNames={()=> {}}/>);
  expect (div.find('button.hidden').text()).toBe('+')
});

it('hides the minus button at 1 dish', () => {
  const div =  shallow(<Stage3 meal={falseModel.meal}
                               numPeople={falseModel.numPeople}
                               restaurant={falseModel.restaurant}
                               dishes={[{name: "", numOrders: 1}]}
                               checkNums={()=> {}}
                               checkNames={()=> {}}/>);
  expect (div.find('button.hidden').text()).toBe('-')
});

it('shows error on name error', () => {
  const div =  mount(<Stage3 meal={falseModel.meal}
                               numPeople={falseModel.numPeople}
                               restaurant={falseModel.restaurant}
                               dishes={falseModel.dishes}
                               checkNums={()=> {return true;}}
                               checkNames={()=> {return false;}}/>);
  expect (div.find('.error')).toHaveLength(falseModel.dishes.length);
});

it('shows error on count error', () => {
  const div =  mount(<Stage3 meal={falseModel.meal}
                               numPeople={falseModel.numPeople}
                               restaurant={falseModel.restaurant}
                               dishes={falseModel.dishes}
                               checkNums={()=> {return false;}}
                               checkNames={()=> {return true;}}/>);
  expect (div.find('.error')).toHaveLength(falseModel.dishes.length);
});

it('successfully calls update function with selected value when dish is selected', ()=>{
  const onDishChange = sinon.spy();
  const div =  mount(<Stage3 updateModel={onDishChange}
                             meal={falseModel.meal}
                             numPeople={falseModel.numPeople}
                             restaurant={falseModel.restaurant}
                             dishes={[{name: "", numOrders: 0}]}
                             checkNums={()=> {}}
                             checkNames={()=> {}}/>);
  var dishName = dishData.getDishDict('','')[0];
  div.find('select').simulate('change', {target: { value: dishName}});
  expect(onDishChange).toHaveProperty('callCount',1);
  expect(onDishChange.calledWith(
  {
    dishes: [{name: dishName, numOrders: 0}]
  }
  )).toBe(true);
});

it('successfully calls update function with input value when orders is entered', ()=>{
  const onOrdersInput = sinon.spy();
  const div =  mount(<Stage3 updateModel={onOrdersInput}
                             meal={falseModel.meal}
                             numPeople={falseModel.numPeople}
                             restaurant={falseModel.restaurant}
                             dishes={[{name: "", numOrders: 0}]}
                             checkNums={()=> {}}
                             checkNames={()=> {}}/>);
  div.find('input').simulate('change', {target: { value: 4}});
  expect(onOrdersInput).toHaveProperty('callCount',1);
  expect(onOrdersInput.calledWith(
  {
    dishes: [{name: "", numOrders: 4}]
  }
  )).toBe(true);
});

it('successfully checks dish names and checks order nums when rendering', ()=>{
  const spyCheckName = sinon.spy();
  const spyCheckNum = sinon.spy();
  const div =  mount(<Stage3 meal={falseModel.meal}
                             numPeople={falseModel.numPeople}
                             restaurant={falseModel.restaurant}
                             dishes={[{name: "", numOrders: 0}]}
                             checkNums={spyCheckNum}
                             checkNames={spyCheckName}/>);
  expect(spyCheckName.called).toBe(true);
  expect(spyCheckNum.called).toBe(true);
});