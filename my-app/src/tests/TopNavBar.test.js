import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, mount } from 'enzyme';
import sinon from 'sinon';
import TopNavBar from '../js/TopNavBar';

it('renders without crashing', () => {
  const div = mount(<TopNavBar />);
});

/*Since this bar is not really designed for extensibility, this test will suffice.
  In a real design situation, all these buttons should have identifiers.
  This would allow the tests to be independant of minor variable changes that may happen as extensions*/

it('does not allow clicking on stages above designated', () => {
  const div = mount(<TopNavBar maxStage={1}
                               currentStage={0}
                               changeStage={()=>{return;}}/>);
  expect(div.find('button[disabled=true]')).toHaveLength(3 - div.props().maxStage);
});

it('successfully manipulates state', ()=>{
  const onHigher = sinon.spy();
  const div =  mount(<TopNavBar maxStage={3}
                               currentStage={0}
                               changeStage={onHigher}/>);
  div.find('button').at(3).simulate('click');
  expect(onHigher).toHaveProperty('callCount',1);
  expect(onHigher.calledWith(3)).toBe(true);
  div.find('button').at(2).simulate('click');
  expect(onHigher).toHaveProperty('callCount',2);
  expect(onHigher.calledWith(2)).toBe(true);
  div.find('button').at(1).simulate('click');
  expect(onHigher).toHaveProperty('callCount',3);
  expect(onHigher.calledWith(1)).toBe(true);
  div.find('button').at(0).simulate('click');
  expect(onHigher).toHaveProperty('callCount',4);
  expect(onHigher.calledWith(0)).toBe(true);
});