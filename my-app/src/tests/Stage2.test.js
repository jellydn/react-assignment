import React from 'react';
import ReactDOM from 'react-dom';
import Stage2 from '../js/Stage2';
import sinon from 'sinon';
import {shallow, mount} from 'enzyme';
import {dishData} from'../js/DishData';

jest.mock('../js/DishData');

beforeAll(() => {
  dishData.getRestaurantDict= (a) =>{
            return ["Anpan","Sparking"];
  };
});

it('renders without crashing', () => {
  const component = mount(<Stage2 />);
});

it('shows error on selection error', () => {
  const div =  mount(<Stage2 meal={falseModel.meal}
                             restaurant=""
                             updateModel={()=>{return;}}/>);
  div.find('select').simulate('change', {target: { value: ""}});
  
  div.update();

  expect (div.find('.error')).toHaveLength(1);
});

it('renders appropriate list items', () => {
  const div =  mount(<Stage2 meal={falseModel.meal}
                             restaurant={falseModel.restaurant}/>);
  expect (div.find('option')).toHaveLength(dishData.getRestaurantDict(null).length+1);
});

it('successfully calls update function with selected restaurant and resets dishes', ()=>{
  const onRestaurantChange = sinon.spy();
  const div =  mount(<Stage2 meal={falseModel.meal}
                             restaurant=""
                             updateModel={onRestaurantChange}/>);
  var restaurantName = dishData.getRestaurantDict('')[0];
  div.find('select').simulate('change', {target: { value: restaurantName}});
  expect(onRestaurantChange).toHaveProperty('callCount',1);
  expect(onRestaurantChange.calledWith(
  {
    restaurant: restaurantName, 
    dishes: [{name: "", numOrders: 1}]
  }
  )).toBe(true);
});
