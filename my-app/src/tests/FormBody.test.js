import React from 'react';
import ReactDOM from 'react-dom';
import { mount } from 'enzyme';
import FormBody from '../js/FormBody';

it('renders without crashing', () => {
  const div = mount(<FormBody maxInputs={falseModel.maxInputs}
                              numPeople={falseModel.numPeople}
                              meal={falseModel.meal}
                              restaurant={falseModel.restaurant}
                              updateModel={()=>{return;}}
                              checkNames={()=>{return;}}
                              checkNums={()=>{return;}}
                              addDish={()=>{return;}}
                              deleteDish={()=>{return;}}
                              dishes={falseModel.dishes}/>);
});

it('applies hidden tag to stages based on given stage', () => {
  const div = mount(<FormBody maxInputs={falseModel.maxInputs}
                              numPeople={falseModel.numPeople}
                              meal={falseModel.meal}
                              restaurant={falseModel.restaurant}
                              updateModel={()=>{return;}}
                              checkNames={()=>{return;}}
                              checkNums={()=>{return;}}
                              addDish={()=>{return;}}
                              deleteDish={()=>{return;}}
                              dishes={falseModel.dishes}
                              currentStage={0}/>);
  expect(div.find('Stage1').props().isHidden).toBe(false);
  expect(div.find('Stage2').props().isHidden).toBe(true);
  expect(div.find('Stage3').props().isHidden).toBe(true);
  expect(div.find('Stage4').props().isHidden).toBe(true);
  div.setProps({currentStage: 1});
  expect(div.find('Stage1').props().isHidden).toBe(true);
  expect(div.find('Stage2').props().isHidden).toBe(false);
  expect(div.find('Stage3').props().isHidden).toBe(true);
  expect(div.find('Stage4').props().isHidden).toBe(true);
  div.setProps({currentStage: 2});
  expect(div.find('Stage1').props().isHidden).toBe(true);
  expect(div.find('Stage2').props().isHidden).toBe(true);
  expect(div.find('Stage3').props().isHidden).toBe(false);
  expect(div.find('Stage4').props().isHidden).toBe(true);
  div.setProps({currentStage: 3});
  expect(div.find('Stage1').props().isHidden).toBe(true);
  expect(div.find('Stage2').props().isHidden).toBe(true);
  expect(div.find('Stage3').props().isHidden).toBe(true);
  expect(div.find('Stage4').props().isHidden).toBe(false);
});