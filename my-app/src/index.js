import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.css';
import PreOrderApplet from './js/App';
import registerServiceWorker from './js/registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(<PreOrderApplet />, document.getElementById('root'));
registerServiceWorker();
